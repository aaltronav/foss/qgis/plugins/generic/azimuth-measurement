# Azimuth and distance measurement

Measure azimuth and distance between two points.

* Choice of geodetic values on the WGS84 ellipsoid or cartesian coordinates.
* Snapping is supported.

![](media/screenshot.png)

This plugin adds a new map tool to QGIS. It can be activated:

* from the [QGIS measurement toolbox](https://docs.qgis.org/latest/en/docs/user_manual/introduction/general_tools.html#measuring) (![](media/mActionMeasure.png));
* from the menu (View → Measure → Measure Azimuth);
* via a keyboard shortcut (Ctrl+Shift+Z)

## Development

Clone the plugin repository and run one of:

```bash
make compile
```

or:

```bash
pip install pb_tool
pb_tool compile
```

## Packaging a new version

```bash
make derase # This will remove the plugin from your QGIS profile.
make zip    # This will create a ZIP file and reinstall the plugin as a side effect.
```

## Credits

The rubber band implementation is based on code from https://github.com/webgeodatavore/azimuth_measurement ([GPL 2.0](https://github.com/webgeodatavore/azimuth_measurement/blob/2ab6db8ea11963bee9c666a77187e68635ff91d2/LICENSE))

The ![](icon.svg) icon is © Haley Halcyon and [distributed](https://github.com/Templarian/MaterialDesign/issues/5777#issuecomment-902664083) under the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) licence.
